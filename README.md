# Hateful Meme Classification

## Introduction
coming soon ... 

## Team Members
- Visshal Natarajan (ISTD, Undergraduate)
- Gabriel Yong (ISTD, Undergraduate)
- Gargi (ISTD, Undergraduate)
- Jaslyn Yee (ISTD, Undergraduate)
- Ming Shan (ISTD, PhD Student)

## Getting Started

### Download Dataset
Download the dataset from [Hateful Memes Challenge](https://hatefulmemeschallenge.com)

### Environment Set-Up
- text2emotion >= 0.05
- many others... coming soon...

### Data Preprocessing
coming soon ...

### Training
coming soon ...

## Experiment Results

|Model| Precision | Recall| F1| Accuracy |
|---|---|---|---|---|
| Gradient Boosted Trees | | | | |
| Random Forest | | | | | |
| ConcatBERT | | | | | |
| VisualBERT | | | | | |

# References
[1] [An Interpretable Approach to Hateful Meme Detection (ICMI'21)](https://arxiv.org/abs/2108.10069)
